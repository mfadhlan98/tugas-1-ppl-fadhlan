import { Inter } from "@next/font/google";
import axios from "axios";
import { useEffect, useState } from "react";
import supabase from "@/lib/supabase";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  const [data, setData] = useState([{ name: "loading..." }]);

  useEffect(() => {
    async function fetch() {
      console.log("fetching...");
      const { data, error } = await supabase.from("kucing").select("kucing_name");
      console.log(error);
      console.log(data)
      setData(data);
    }
    fetch();
  }, []);

  return (
    <>
      <h1>Hello World!</h1>
      <p>Kucing-kucing yang saya miliki</p>
      {data.map((item, i) => (
        <h3 key={i}>{item.kucing_name}</h3>
      ))}
    </>
  );
}
